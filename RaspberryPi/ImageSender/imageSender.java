import java.net.Socket;
import java.io.OutputStream;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.nio.ByteBuffer;
import java.io.ByteArrayInputStream;
import java.net.ServerSocket;
import javax.imageio.ImageIO;
import java.io.File;

public class imageSender {

    public static void main(String[] args) throws Exception {
        Socket socket = new Socket("10.3.141.117", 13085);

        OutputStream outputStream = socket.getOutputStream();

        BufferedImage image = ImageIO.read(new File("test.jpg"));

        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        ImageIO.write(image, "jpg", byteArrayOutputStream);

        byte[] size = ByteBuffer.allocate(4).putInt(byteArrayOutputStream.size()).array();
        outputStream.write(size);
        outputStream.write(byteArrayOutputStream.toByteArray());
        outputStream.flush();
        System.out.println("Flushed: " + System.currentTimeMillis());

        Thread.sleep(120000);
        System.out.println("Closing: " + System.currentTimeMillis());
        socket.close();
    }
}
