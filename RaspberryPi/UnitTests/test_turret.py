#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
To be used on the Raspberry Pi
Ask the turret if we have been hit.
"""
import smbus

bus = smbus.SMBus(1)

try:
    hitStatus = bus.read_byte_data(0x04, 0)
    print(hitStatus)
    if hitStatus:
        print (' Hit !')
except OSError:
    print (' No turret ')
