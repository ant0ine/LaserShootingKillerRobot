#include <Wire.h>
#include <PinChangeInt.h>

/*I2C Storage array*/
int I2C_Storage [4]{0,0,0,0};

/*Output variables*/
int filter_left [10]{};
int filter_right [10]{};
int sum_left = 0;
int sum_right = 0;

/*Watchdogvariables to know if I2C connection is lost*/
long connection_watchdog  = 0;
long last_transmission    = 0;

/* bytes recieved from I2C
 *Arm bit (Status_byte<0>), is checked before output is written. If 0 no output,
 * if 1 output allowed. Direction bit Left (Status_byte<1>) if 1 forward 
 *movement, if 0 backwards. Direction bit Right (Status_byte<2>) if 0 forward 
 *movement, if 1 backwards. 
 */
int Status_byte     = 0;
int Throttle_Left   = 0; 
int Throttle_Right  = 0; 

/* variables for PID feedbackloop*/ 
/*
int Speed_Left      = 0;      
int Speed_Right     = 0;
int Error_Left      = 0;
int Error_Right     = 0;
*/

/* Output value to control the velocity of the wheels (0 - 255))*/
int Output_Left     = 0;
int Output_Right    = 0;

/* Interrupt variables for Encoders */ //(<-- use if Encoders are inplemented)
/*
volatile double Enc1_value = 0;
volatile double Enc2_value = 0;
volatile double Enc3_value = 0;
volatile double Enc4_value = 0;
volatile double t_prev = 0;
uint8_t latest_interrupted_pin;
*/
      
/* Use this to set the maximum time the MC continues the output withhout 
 * connection. e.g.: for a sampling rate of 50Hz and a maximum of 5 lost values 
 * use 100
 */
#define max_time_without_command 500

/*Status LEDs */
#define LED_pin_RED     2

/* Pins for H-bridges
 * Enable pins are used for speed control with PWM 
 * Input 1 and 2 are used to break and direction control, see L298 datasheet, 
 * (https://www.sparkfun.com/datasheets/Robotics/L298_H_Bridge.pdf). 
 */
#define Enable_A        9     
#define Input_A_1       8
#define Input_A_2       7
#define Enable_B        10
#define Input_B_1       11
#define Input_B_2       12

/* Pins of the encoder */ /*(<-- must be checked before Encoders are implemented)
#define Encoder_1A      4
#define Encoder_1B      5
#define Encoder_2A      6
#define Encoder_2B      7
*/

/* Filter length */
#define filter_i 7
int t =0;

/*_________________________________________________________
 * Main 
 * Christopher McClanahan, 12.2017
 * 
 * Motor Controller (MC) for Embedded Systems Design Projct.
 * This program controlls four motors via two channels, a filter is used to 
 * smoothen the motoroutput and to prevent to high currents after velocity 
 * changes. The desired speed comes from a control unit via I2C. If the MC 
 * doesn't receive a command in a set time, it will disable the motors, to 
 * prevent uncontrolled movements during an I2C timeout. To activate the motors, 
 * the MC expects a HIGH arm flag, sent in the statusbyte.
 *__________________________________________________________*/

void setup() {
    Serial.begin (57600);
    Serial.println ("Starting");
   
    /*LED status indicator. */
    pinMode(LED_pin_RED, OUTPUT); 

    /*Hello World! */
    digitalWrite(LED_pin_RED,HIGH);
    delay(500);
    digitalWrite(LED_pin_RED,LOW);
  
    /*pins for H-bridges */
    pinMode(Enable_A, OUTPUT); 
    pinMode(Enable_B, OUTPUT); 
    pinMode(Input_A_1, OUTPUT); 
    pinMode(Input_A_2, OUTPUT); 
    pinMode(Input_B_1, OUTPUT); 
    pinMode(Input_B_2, OUTPUT); 
  
    /*setting the interrupt pin as an input and activate the internal pullup*/
    //  pinMode(Encoder_1A , INPUT); digitalWrite(Encoder_1A , HIGH); 

  
    /*say Pins initialised!!*/
    Serial.println ("Pins initialised");
    
    digitalWrite(LED_pin_RED, HIGH);
    delay(80);
    digitalWrite(LED_pin_RED, LOW);
    delay(80);
    digitalWrite(LED_pin_RED, HIGH);
    delay(80);
    digitalWrite(LED_pin_RED, LOW);
    delay(80);
    digitalWrite(LED_pin_RED,HIGH);
    delay(500);
    digitalWrite(LED_pin_RED,LOW);
    delay(100);
      
    /* Initialising I2C */
    Wire.begin(3);                  // set Motorcontroller address to 3               
    Wire.onReceive(receiveEvent);   // register event
    Serial.println ("I2C initialised");

    /*Initialising interrups */
    //  PCintPort::attachInterrupt(Encoder_1A, &rising, RISING);
    //  Serial.println("Interrupts initialised");

    /*Wait for other systems to initialize e.g. wait for transmitter to establish 
     *connection. */
    while (t<8){
        digitalWrite(LED_pin_RED, HIGH);
        delay(40);
        digitalWrite(LED_pin_RED, LOW);
        delay(40);
        t = millis()/1000;
    }
}

/*_________________________________________________________*/

void loop() {

    /* Check if the I2C connection is lost */ 
    check_I2C_connection();

    /* Calculate actual rpm from encoder times */
    //  calc_rpm(); 

    /* Calculate output rpm */
    control_calc();

    /* Convert output rpm and write outputs */
    write_outputs();

    delay(10);  // slow down the statemachine
}
