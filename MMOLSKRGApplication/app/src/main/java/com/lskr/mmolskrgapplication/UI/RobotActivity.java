package com.lskr.mmolskrgapplication.UI;


import android.media.MediaPlayer;
import android.os.AsyncTask;
import android.os.Bundle;
import android.app.Activity;
import android.util.DisplayMetrics;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.SeekBar;
import android.widget.TextView;

import com.lskr.mmolskrgapplication.Connection.ReadingTask;
import com.lskr.mmolskrgapplication.Connection.WritingTask;
import com.lskr.mmolskrgapplication.Constants;
import com.lskr.mmolskrgapplication.Robot.Robot;
import com.lskr.mmolskrgapplication.R;

/**
 * Created by Julien on 01/11/2017.
 */

/**
 * this class implements an activity object. This is our main UI thread. It is used when the
 * button mode is selected by the user. It will load the xml file that describe the graphical
 * interface, create a robot object used to gather the data to be sent to the rover and launch two
 * threads. One to send data to the rover (WritingTask) and one to read data from the rover
 * (ReadingTask)
 */

public class RobotActivity extends Activity {

    Button fireBtn;
    Button forwardBtn;
    Button backwardBtn;
    Button upBtn;
    Button downBtn;
    SeekBar throttle;
    Robot robot;
    TextView direction;
    TextView thrust;
    TextView fire;
    MediaPlayer mp;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,WindowManager.LayoutParams.FLAG_FULLSCREEN);
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);

        DisplayMetrics dm = new DisplayMetrics();

        //getting screen width and height
        getWindowManager().getDefaultDisplay().getMetrics(dm);
        Constants.SCREEN_WIDTH = dm.widthPixels;
        Constants.SCREEN_HEIGHT = dm.heightPixels;
        Constants.CURRENT_CONTEXT = this;

        createRobot();

        setContentView(R.layout.activity_lskrapp);

        forwardBtn  =(Button)findViewById(R.id.forward);
        backwardBtn =(Button)findViewById(R.id.backward);
        fireBtn     = (Button)findViewById(R.id.fire);
        upBtn       = (Button)findViewById(R.id.turretUp);
        downBtn     = (Button)findViewById(R.id.turretDown);
        throttle    = (SeekBar)findViewById(R.id.thrust);

        direction   = (TextView)findViewById(R.id.direction);
        thrust      = (TextView)findViewById(R.id.thrustValue);
        fire        = (TextView)findViewById(R.id.fireMessage);

        mp = MediaPlayer.create(this,R.raw.blaster);

        // linking all buttons to their listeners function
        forwardBtn.setOnTouchListener(forwardListener);
        backwardBtn.setOnTouchListener(backwardListener);
        fireBtn.setOnTouchListener(fireListener);
        upBtn.setOnTouchListener(turretUpListener);
        downBtn.setOnTouchListener(turretDownListener);
        throttle.setOnSeekBarChangeListener(thrustListener);

        direction.setText(robot.getDirection());
        thrust.setText(String.valueOf(robot.getThrust()));

        // Allow the robot to move whenever the user push a button
        robot.setMove(1);

        // launching the two communication threads
        new WritingTask().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR,robot);
        new ReadingTask().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR,"");



    }



    public void createRobot(){
        Bundle intentExtras = getIntent().getExtras();
        //RobotControlMode = (RobotControlMode) intentExtras.get(Constants.CONTROL_MODE);
        robot = new Robot();// new Robot(RobotControlMode);

    }


    /* Button listeners
    * All listeners will have an action on the robot object
    * parameters. They will be triggered when the user will touch a button
    * In the following methods, ACTION_DOWN indicates that the user push the button and hold it.
    * ACTION_UP indicates the moment when the user release the button
    */
    private OnTouchListener forwardListener = new OnTouchListener(){
        @Override
        public boolean onTouch(View v,MotionEvent event){
            switch(event.getAction()){
                case MotionEvent.ACTION_DOWN:
                    robot.setDirection("1");
                    direction.setText("forward");
                    break;
                case MotionEvent.ACTION_UP:
                    robot.setDirection("0");
                    direction.setText("idle");
                    break;
            }
            return true;
        }

    };

    private OnTouchListener backwardListener = new OnTouchListener(){
        @Override
        public boolean onTouch(View v,MotionEvent event){
            switch(event.getAction()){
                case MotionEvent.ACTION_DOWN:
                    robot.setDirection("2");
                    direction.setText("backward");
                    break;
                case MotionEvent.ACTION_UP:
                    robot.setDirection("0");
                    direction.setText("idle");
                    break;
            }
            return true;
        }

    };


    private OnTouchListener turretUpListener = new OnTouchListener(){
        @Override
        public boolean onTouch(View v,MotionEvent event){
            switch(event.getAction()){
                case MotionEvent.ACTION_DOWN:
                    robot.setTurret_pos(2);
                    break;
                case MotionEvent.ACTION_UP:
                    robot.setTurret_pos(0);
                    break;
            }
            return true;
        }

    };


    private OnTouchListener turretDownListener = new OnTouchListener(){
        @Override
        public boolean onTouch(View v,MotionEvent event){
            switch(event.getAction()){
                case MotionEvent.ACTION_DOWN:
                    robot.setTurret_pos(1);
                    break;
                case MotionEvent.ACTION_UP:
                    robot.setTurret_pos(0);
                    break;
            }
            return true;
        }

    };


    private View.OnTouchListener fireListener = new View.OnTouchListener() {
        @Override
        public boolean onTouch(View view, MotionEvent event) {
            switch(event.getAction()){
                case MotionEvent.ACTION_DOWN:
                    robot.setFire("1");
                    fire.setText("FIRE");
                    fire.setVisibility(View.VISIBLE);
                    mp.start();
                    break;
                case MotionEvent.ACTION_UP:
                    robot.setFire("0");
                    fire.setVisibility(View.INVISIBLE);
                    break;
            }
            return true;
        }
    };



    // seekbar listener to know the throttle
    private SeekBar.OnSeekBarChangeListener thrustListener = new SeekBar.OnSeekBarChangeListener() {
        @Override
        public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
            thrust.setText(String.valueOf(seekBar.getProgress()));
        }

        @Override
        public void onStartTrackingTouch(SeekBar seekBar) {

        }

        @Override
        public void onStopTrackingTouch(SeekBar seekBar) {
            robot.setThrust(seekBar.getProgress());
            thrust.setText(String.valueOf(seekBar.getProgress()));
        }
    };

}
