#!/usr/bin/env python3
# coding: utf-8

"""
Test of the laser canon.
To be used on the Raspberry Pi
"""

import smbus
from time import sleep

bus = smbus.SMBus(1)

# Fire
bus.send_i2c_block(0x5, 1, [0, 0, 0])
bus.send_i2c_block(0x5, 1, [0, 0, 0])
sleep(1)

# Move to one direction (up)
bus.send_i2c_block(0x5, 0, [1, 0, 0])
bus.send_i2c_block(0x5, 0, [1, 0, 0])
bus.send_i2c_block(0x5, 0, [1, 0, 0])
bus.send_i2c_block(0x5, 0, [1, 0, 0])
bus.send_i2c_block(0x5, 0, [1, 0, 0])
bus.send_i2c_block(0x5, 0, [1, 0, 0])
bus.send_i2c_block(0x5, 0, [1, 0, 0])
sleep(1)

# Move other direction (down)
bus.send_i2c_block(0x5, 0, [2, 0, 0])
bus.send_i2c_block(0x5, 0, [2, 0, 0])
bus.send_i2c_block(0x5, 0, [2, 0, 0])
bus.send_i2c_block(0x5, 0, [2, 0, 0])
bus.send_i2c_block(0x5, 0, [2, 0, 0])
bus.send_i2c_block(0x5, 0, [2, 0, 0])

