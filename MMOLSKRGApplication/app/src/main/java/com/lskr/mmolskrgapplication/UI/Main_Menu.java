package com.lskr.mmolskrgapplication.UI;

import android.content.Intent;
import android.os.Bundle;
import android.app.Activity;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.lskr.mmolskrgapplication.Constants;
import com.lskr.mmolskrgapplication.R;

/**
 * Created by Julien on 01/11/2017.
 */


/**
 * this class implements an activity object. It is our main menu. It will load the xml file that
 * describes the UI. When the start button is pressed it launches one of our two control activity,
 * RobotActivity or RobotActivitySensor
 */

public class Main_Menu extends Activity {

    public Button startButton;
    public RadioGroup mode;
    public EditText ip;
    public EditText port;
    public TextView field_missing;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_menu);

        startButton = (Button)findViewById(R.id.startButton);

        mode = (RadioGroup)findViewById(R.id.mode);

        ip = (EditText)findViewById(R.id.IPEdit);

        port = (EditText)findViewById(R.id.PortEdit);

        field_missing =(TextView)findViewById(R.id.Field_missing);

        startButton.setOnClickListener(startListener);
    }

    private OnClickListener startListener = new View.OnClickListener(){
        @Override
        public void onClick(View v) {

            if (port.getText().toString().isEmpty()  || ip.getText().toString().isEmpty()){
               field_missing.setVisibility(View.VISIBLE);
            }

            else{
                Constants.PORT = port.getText().toString();
                Constants.IP = ip.getText().toString();

                /*
                * checking which mode has been chosen and launch the corresponding activity
                */
                if (mode.getCheckedRadioButtonId() == R.id.button_mode) {
                    Constants.GAME_MODE = 1;
                    Intent myIntent = new Intent(Main_Menu.this, RobotActivity.class);
                    startActivity(myIntent);
                }
                else {
                    Constants.GAME_MODE = 0;
                    Intent myIntent = new Intent(Main_Menu.this, RobotActivitySensor.class);
                    startActivity(myIntent);
                }
            }
        }
    };
}
