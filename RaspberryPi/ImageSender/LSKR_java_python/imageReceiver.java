import java.net.Socket;
import java.io.InputStream;
import java.awt.image.BufferedImage;
import java.nio.ByteBuffer;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.net.ServerSocket;
import javax.imageio.ImageIO;
import java.io.File;
import java.lang.Integer;
import java.util.ArrayList;



public class imageReceiver{
    public static int bufferSize;


    
    public static void main(String[] args) throws Exception {
        Socket socket = new Socket(args[0],Integer.parseInt(args[1]));
        bufferSize = 1024;

        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        InputStream inputStream = socket.getInputStream();
        System.out.println("Reading: " + System.currentTimeMillis());


        // receiving total size of image
        byte[] sizeAr = new byte[4];
        inputStream.read(sizeAr);
        int size = ByteBuffer.wrap(sizeAr).asIntBuffer().get();

        //number of complete table of 1024 bytes
        int numberTable = (int)size/bufferSize;
        // last package of data of image
        int left = size%bufferSize; 
        /*
        System.out.println(size);
            byte[] imageAr = new byte[size];
            inputStream.read(imageAr);
        */

        // preparing out total image byte array
        

        // receivng packages one by one and adding it to a total byte array
        for (int i = 0; i < numberTable; i++)
        {
            byte[] imageAr = new byte[bufferSize];
            inputStream.read(imageAr);
            outputStream.write(imageAr);
        }
        
        byte[] imageAr = new byte[left];
        inputStream.read(imageAr);
        outputStream.write(imageAr);
        // concatenate image with image total
        byte imageTotalAr[]= outputStream.toByteArray(); 

        BufferedImage image = ImageIO.read(new ByteArrayInputStream(imageTotalAr));

        System.out.println("Received " +  image.getWidth() + "x" + image.getHeight() + ": " + System.currentTimeMillis());
        ImageIO.write(image, "jpg", new File("test2.jpg"));

        socket.close();
    }

}


