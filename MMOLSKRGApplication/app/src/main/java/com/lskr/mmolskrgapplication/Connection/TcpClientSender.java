package com.lskr.mmolskrgapplication.Connection;

import android.util.Log;
import com.lskr.mmolskrgapplication.Constants;
import com.lskr.mmolskrgapplication.Robot.Robot;
import java.io.BufferedWriter;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.InetAddress;
import java.net.Socket;

/**
 * Created by Julien on 07/11/2017.
 */

/**
 * This class implements a client using TCP/IP protocole. This client is designed to send data to
 * the server with a defined timing
 */

public class TcpClientSender {
    public static final String SERVER_IP = Constants.IP; //your computer IP address
    public static final int SERVER_PORT = Integer.parseInt(Constants.PORT);
    // message to send to the server
    private Robot mRobot;
    // while this is true, the server will continue running
    private boolean mRun = false;
    // used to send messages
    private PrintWriter mBufferOut;
    private boolean SockAv = false;
    private Socket socket;
    private long time1;
    private long time2;

    /**
     * Constructor of the class
     */
    public TcpClientSender(Robot robot) {
        mRobot = robot;
    }

    /**
     * Sends the message entered by client to the server
     */
    public void sendMessage(String message) {
        if (mBufferOut != null && !mBufferOut.checkError()) {
            mBufferOut.println(message);
            mBufferOut.flush();
        }
    }

    /**
     * Close the connection and release the members
     */
    public void stopClient() {
        mRun = false;

        if (mBufferOut != null) {
            mBufferOut.flush();
            mBufferOut.close();
        }

        mBufferOut = null;
    }

    public void run() {
        mRun = true;

        try {
            //Server IP Address
            InetAddress serverAddr = InetAddress.getByName(SERVER_IP);

            Log.e("TCP ClientSender", "C: Connecting...");

            //create a socket to make the connection with the server
            socket = new Socket(serverAddr, SERVER_PORT);
            // create the object to write in the socket
            mBufferOut = new PrintWriter(new BufferedWriter(new OutputStreamWriter(socket.getOutputStream())), true);

            SockAv = true;

        } catch (Exception e) {
            Log.e("TCP", "C: Error", e);
        }

        if (SockAv == true){
            sendMessage("o");
            time1 = System.currentTimeMillis();
            time2 = System.currentTimeMillis();
            while (mRun) {
                time2 = System.currentTimeMillis();
                if (time2-time1 >=50.0) {  // send a message every 50ms
                    time1 = time2;
                    sendMessage(mRobot.generateMessage());
                }
            }
        }
    }
}
