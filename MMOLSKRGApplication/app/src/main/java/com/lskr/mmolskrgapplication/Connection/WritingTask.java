package com.lskr.mmolskrgapplication.Connection;

import android.os.AsyncTask;
import android.util.Log;
import com.lskr.mmolskrgapplication.Robot.Robot;

/**
 * Created by Julien on 02/11/2017.
 * This class implement an asynchronous task (a thread) which will be used to send data to the RasPi
 * avoiding blocking the UI
 */

public class WritingTask extends AsyncTask<Robot,String,TcpClientSender> {
    public TcpClientSender mTcpClient;
    public Robot mRobot;

    @Override
    protected TcpClientSender doInBackground(Robot... robot) {
        //we create a TCP client object able to send data, with our robot object as argument
        mRobot = robot[0];
        mTcpClient = new TcpClientSender(mRobot);
        System.out.println("writing");
        // launching the thread
        mTcpClient.run();

        return null;
    }

    // publish function only used for debug
    @Override
    protected void onProgressUpdate(String... values) {
        super.onProgressUpdate(values);

        Log.i("onProgressUpdate", values[0]);
    }
}

