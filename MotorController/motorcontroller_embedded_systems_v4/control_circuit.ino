/*_________________________________________________________
 * Controller 
 * Christopher McClanahan, 11.2017
 * 
 * The desired and actual angular velocity is filtered here, to prevent too high
 * currents caused by high accelerations and decelerations. If a feedback 
 * controller is implemented instead, place it here.
 __________________________________________________________*/


/*this is not a feedbackloop*/
void control_calc (){
    /*get rid of old data */
    sum_left = 0;
    sum_right = 0;

    /*update filter array and sum the content*/
    for (int i=filter_i; i >= 0; i--){
        filter_left[i] = filter_left[i-1];
        filter_right[i] = filter_right[i-1];
        
        if(i==0){
            filter_left[i] = Throttle_Left;
            filter_right[i] = -Throttle_Right;
        }
        sum_left = sum_left + filter_left[i];
        sum_right = sum_right + filter_right[i];
    }
    Serial.print("control");
    /*calculate average of filter for outputting */
    Output_Left = sum_left/filter_i;
    Output_Right = sum_right/filter_i;
    
    /* // for debugging
    Serial.print("   Output_Right ");
    Serial.print(Output_Right);
    Serial.print("\t");
    Serial.print("   Output_Left ");
    Serial.print(Output_Left);
    Serial.print("\t");*/
}
