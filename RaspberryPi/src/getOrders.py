#!/usr/bin/env python2
# -*- coding: utf-8 -*-

"""
Main get Orders to be used with the fsm.py file.

/!\ As it is, this will not work on standalone
"""

# from fsm import FSM
import time
import struct
import numpy as np

def readlineSocket(socket):
    """
    This function reads byte per byte the given socket until the \n character
    is received. 
    It returns a concatenated string.
    """
    receivedOrder = ""
    receivedByte = ""
    while receivedByte != '\n':
        receivedOrder += receivedByte
        a = socket.recv(1)
        try:
            receivedByte = chr(struct.unpack('!b',a)[0])
        except:
            receivedByte = ""

    return receivedOrder


def mainGetOrders(socketReceiveOrders, OrderQ, timeoutQ, stopFlag):
    """
    Main thread to handle reading in the socket when we get orders.
    The read function is locking the thread. Using a queue object prevents
    the mainInteractions thread to be locked.

    NB: Stop flag porbably useless if used with subprocess and not threading in
    fsm.py
    """
    print( 'THREAD GET ORDERS OK' )
    while not stopFlag:
        receivedOrder = readlineSocket(socketReceiveOrders)
        if receivedOrder == 'stop':
            OrderQ.put('shutdown')
        if receivedOrder != '':
            print('\t\t\tOrders',receivedOrder)
            OrderQ.put(receivedOrder)

            # For safety purpose
            timeoutQ.put(1)
    print('OUT OF Get Orders')

if __name__ == '__main__':
    print("Not to be used that way :p\n(and to lazy to write some unit tests)")
