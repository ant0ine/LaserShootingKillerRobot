// Laser sensing for target
// Laser shooting killer robot project
// December 2017 

// Turns LED off when the button is pressed and the target is "reset"

#include <Wire.h>

// Pin declarations
int inputPin0 = A0;
int inputPin1 = A1; 
int inputPin2 = A2; 
int inputPin3 = A3; 
//int inputPin4 = A4; 
//int inputPin5 = A5;

int led = 6; 
int button = 7;
boolean receivedRequest = false;

// Variables to store value
int inputVoltage0 = 0; 
int inputVoltage1 = 0; 
int inputVoltage2 = 0; 
int inputVoltage3 = 0; 
//int inputVoltage4 = 0; 
//int inputVoltage5 = 0; 

int buttonState = 0;
int ledState = 0; 

void setup() 
{
    Serial.begin(9600);
    
    pinMode(led, OUTPUT);
    digitalWrite(led, LOW); // LED is low at setup 
    pinMode(button, INPUT);
    
    Wire.begin(4);                // join i2c bus with address #4
    Wire.onRequest(targetHit);   // register event
    Wire.onReceive(readI2CEvent);
    Serial.println(" Boot complete !");
}

void loop() 
{
    // Reading of input voltage from the different pins 
    inputVoltage0 = analogRead(inputPin0);
    inputVoltage1 = analogRead(inputPin1);
    inputVoltage2 = analogRead(inputPin2);
    inputVoltage3 = analogRead(inputPin3);

    // Disable because i2c uses these pins.
    // Try to solder more LDR in parallel to save some analog inputs ports.
    // inputVoltage4 = analogRead(inputPin4);
    // inputVoltage5 = analogRead(inputPin5); 
    
    buttonState = digitalRead(button);
    ledState = digitalRead(led);
    
    // Turns the LED on when the laser hits the target 
    if (inputVoltage0 > 900 || 
        inputVoltage1 > 900 || 
        inputVoltage2 > 900 || 
        inputVoltage3 > 900  ) 
    {
        digitalWrite(led, HIGH);
    }
    
    // Turns the led off if button is pressed
    if (buttonState == HIGH)
    {
        digitalWrite(led, LOW);
    }
    delay(50);
}

// Sends the value of the led to the master, when requested 
// 1 = target is hit
// 0 = target is not hit 
void targetHit()
{
    Wire.write(ledState); 
}

// This event seems to have a higher priority than the "write" one because
// it is launched at every i2c request
void readI2CEvent(int howmany)
{
    Serial.print("test = ");
    int i = Wire.read();
    Serial.println(i);
}


