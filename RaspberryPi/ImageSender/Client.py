#! /usr/bin/env python2
# -*- coding: utf-8 -*-

import socket
import cv2
import numpy as np
import threading
from math import cos,sin,radians

def nothing(x):
    pass

class affthread(threading.Thread):
    """
    This thread handles the camera feedback and the transmission of the command
    information. 
    - Use Z Q S D to move and A to stop
    - The slider set the speed
    """
    def __init__(self,address):
        threading.Thread.__init__(self)

        # Socket connection for the image transmission
        self.s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        print("waiting for 1st connection")
        self.s.connect((address, 1111))
        print("IMAGE connected")
        self.s.send('video')

        # Socket connection for the control command transmission
        self.s2 = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        print("waiting for 2nd connection")
        self.s2.connect((address, 1111))
        print("CONTROL connected")
        self.s2.send('order')

        # Socket connection for the control command transmission
        self.s3 = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        print("waiting for 3nd connection")
        self.s3.connect((address, 1111))
        print("LOGS connected")
        self.s3.send('logs')
    
    def run(self):
        frame3=np.zeros([480,640,3])
        cv2.namedWindow('FPV Feedback')
        cv2.createTrackbar('Speed','FPV Feedback',0,2000,nothing)
        while True:
            r = self.s.recv(999999)#récupération de l'envoi
            frame = np.fromstring (r,dtype=np.uint8)
            frame2 = cv2.imdecode(frame,1)#décodage de l'image depuis la compression jpg
            try:#des erreurs de transmission peuvent apparaitre. un freeze image est mis en place.
                pix = frame2[0,0,0]
            except:
                frame2 = frame3
            cv2.imshow('FPV Feedback',frame2)
            k = cv2.waitKey(1) & 0xFF
            #attention, l'appui sur une touche est bloquant
            speed = cv2.getTrackbarPos('Speed','FPV Feedback')
            a="xs"
            if k == 27:
                self.s2.send("x00")
                break
            elif k==122:
                a="z"#+str(speed)
            elif k==115:
                a="s"#+str(speed)
            elif k==113:
                a="q"#+str(speed)
            elif k==100:
                a="d"#+str(speed)
            elif k==97:
                a="a1600"

            if a!="xs":
                self.s2.send(a)
            frame3=frame2
        #après un appui sur ECHAP, tout s'arrete.
        # self.s.send('y')
        # self.s2.send('y')
        # self.s.close()
        # self.s2.close()
        self.s2.sendall('shutdown')
        self.s.shutdown(socket.SHUT_RDWR)
        self.s2.shutdown(socket.SHUT_RDWR)
        self.s3.shutdown(socket.SHUT_RDWR)
        cv2.destroyAllWindows()

if __name__ == '__main__':
    import sys

    #address="localhost"
    address = sys.argv[1]
    aff = affthread(address)
    aff.start()
        
