#!/usr/bin/env python2
# -*- coding: utf-8 -*-

"""
Used to test the socket features, the multi threading of the server and the
use of shared variables between threads/processes
"""

import socket
import time
import struct

# s1 = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
# s1.connect(("localhost", 1111))
# s1.send('v')
# print ('1')

# time.sleep(2)
s2 = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s2.connect(("localhost", 1111))
s2.send('o')
print ('2')

time.sleep(1)
s3 = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s3.connect(("localhost", 1111))
s3.send('l')
print ('3')


time.sleep(0.51)
s2.send('djfksjf')
print('ok1')

while True:
    time.sleep(0.5)
    print('sending')
    s2.send('COUCOUCOUOUCOUOUCOUOUOUOU')
print('ok2')
