import java.io.BufferedReader;
import java.io.FileOutputStream;
import java.io.InputStreamReader;
import java.io.InputStream;
import java.io.FileInputStream;
import java.io.PrintWriter;
import java.io.BufferedInputStream;
import java.net.Socket;


public class client
{
    public static void main(String[] args) throws Exception
    {
        String usr2ConnectDefault = "10.3.141.150";
        int port2ConnectDefault = 5003;
        Socket socket;
        BufferedReader br;
        PrintWriter out;
	InputStream in;

        socket = new Socket(usr2ConnectDefault, port2ConnectDefault);
        System.out.println("Connected to server...sending echo string");

        br = new BufferedReader(new InputStreamReader(socket.getInputStream()));
        out = new PrintWriter(socket.getOutputStream(),true);
	in = socket.getInputStream();

	byte[] sizeAr = new byte[4];
        in.read(sizeAr);
        int size = ByteBuffer.wrap(sizeAr).asIntBuffer().get();

	
        // say hello to server
        out.println("Hello");
        // read hello from server
        String readString = in.readLine();
        System.out.println(readString);

        out.println("ReadFile");

	
        // verify if request is OK
	
	readString = br.readLine();
        if(readString.compareToIgnoreCase("OK") == 0)
            System.out.println("Receive new file!!!");
        else
        {
            socket.close();
            return;
        }

        // get size of file
        readString = br.readLine();
        int sizeOfFile = Integer.parseInt(readString);
        //InputStream is = socket.getInputStream();
        byte[] fileData = new byte[sizeOfFile];

        for(int i = 0; i < sizeOfFile; i++)
        {
            fileData[i] = (byte)inbinary.read();
        }

        // save file to disk
        FileOutputStream fos = new FileOutputStream("fileImage.jpg");
        try
        {
            fos.write(fileData);
        }
        finally {
            fos.close();
        }
	
        // verify if request is OK
        readString = br.readLine();
        if(readString.compareToIgnoreCase("OK") == 0)
            System.out.println("New file received!!!");
        else
        {
            socket.close();
            return;
        }

        socket.close();
    }
}
