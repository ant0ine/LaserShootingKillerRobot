package com.lskr.mmolskrgapplication.UI;

import android.media.MediaPlayer;
import android.os.AsyncTask;
import android.os.Bundle;
import android.app.Activity;
import android.util.DisplayMetrics;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;

import com.lskr.mmolskrgapplication.Connection.ReadingTask;
import com.lskr.mmolskrgapplication.Connection.WritingTask;
import com.lskr.mmolskrgapplication.Constants;
import com.lskr.mmolskrgapplication.R;
import com.lskr.mmolskrgapplication.Robot.Robot;

/**
 * Created by Julien on 06/11/2017.
 */


/**
 * this class implements an activity object. This is our main UI thread. It is used when the
 * sensor mode is selected by the user. It will load the xml file that describe the graphical
 * interface, create a robot object used to gather the data to be sent to the rover and launch two
 * threads. One to send data to the rover (WritingTask) and one to read data from the rover
 * (ReadingTask)
 */
public class RobotActivitySensor extends Activity{
    Button fireBtn;
    Button upBtn;
    Button downBtn;
    Button movingBtn;
    TextView direction;
    TextView thrust;
    private TextView fire;
    MediaPlayer mp;
    Robot mRobot;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,WindowManager.LayoutParams.FLAG_FULLSCREEN);
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);

        DisplayMetrics dm = new DisplayMetrics();

        //getting screen width and height
        getWindowManager().getDefaultDisplay().getMetrics(dm);
        Constants.SCREEN_WIDTH = dm.widthPixels;
        Constants.SCREEN_HEIGHT = dm.heightPixels;

        setContentView(R.layout.activity_lskrapp_sensor);

        Constants.CURRENT_CONTEXT = this;

        // create a new robot object in the activity
        createRobot();

        // linking xml code to actual buttons
        fireBtn   = (Button)findViewById(R.id.fire);
        upBtn     = (Button)findViewById(R.id.turretUp);
        downBtn   = (Button)findViewById(R.id.turretDown);
        movingBtn = (Button)findViewById(R.id.moving);

        direction = (TextView)findViewById(R.id.direction);
        thrust    = (TextView)findViewById(R.id.thrustValue);
        fire      = (TextView)findViewById(R.id.fireMessage);
        //speed     = (TextView)findViewById(R.id.speed);

        // music player object
        mp = MediaPlayer.create(this,R.raw.blaster);

        direction.setText(mRobot.getDirection());
        thrust.setText(String.valueOf(mRobot.getThrust()));

        // linking all buttons to their listeners function
        fireBtn.setOnTouchListener(fireListener);
        upBtn.setOnTouchListener(turretUpListener);
        downBtn.setOnTouchListener(turretDownListener);
        movingBtn.setOnTouchListener(movingListener);

        // launching the two communication threads
        new WritingTask().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR,mRobot);
        new ReadingTask().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR,"");
    }

    public void createRobot(){
        Bundle intentExtras = getIntent().getExtras();
        mRobot = new Robot();// new Robot(RobotControlMode);
    }

    /* Button listeners
    * All listeners will have an action on the robot object
    * parameters. They will be triggered when the user will touch a button
    * In the following methods, ACTION_DOWN indicates that the user push the
    * button and hold it.
    * ACTION_UP indicates the moment when the user release the button
    */
    private OnTouchListener turretUpListener = new OnTouchListener(){
        @Override
        public boolean onTouch(View v,MotionEvent event){
            switch(event.getAction()){
                case MotionEvent.ACTION_DOWN:
                    mRobot.setTurret_pos(2);
                    break;
                case MotionEvent.ACTION_UP:
                    mRobot.setTurret_pos(0);
                    break;
            }
            return true;
        }
    };

    private OnTouchListener turretDownListener = new OnTouchListener(){
        @Override
        public boolean onTouch(View v,MotionEvent event){
            switch(event.getAction()){
                case MotionEvent.ACTION_DOWN:
                    mRobot.setTurret_pos(1);
                    break;
                case MotionEvent.ACTION_UP:
                    mRobot.setTurret_pos(0);
                    break;
            }
            return true;
        }
    };

    private OnTouchListener movingListener = new OnTouchListener(){
        @Override
        public boolean onTouch(View v,MotionEvent event){
            switch(event.getAction()){
                case MotionEvent.ACTION_DOWN:
                    mRobot.setMove(1);
                    break;
                case MotionEvent.ACTION_UP:
                    mRobot.setMove(0);
                    break;
            }
            return true;
        }
    };

    private View.OnTouchListener fireListener = new View.OnTouchListener() {
        @Override
        public boolean onTouch(View view, MotionEvent event) {
            switch(event.getAction()){
                case MotionEvent.ACTION_DOWN:
                    mRobot.setFire("1");
                    // displaying fire message on the screen
                    fire.setText("FIRE");
                    fire.setVisibility(View.VISIBLE);
                    // playing sound
                    mp.start();
                    break;

                case MotionEvent.ACTION_UP:
                    mRobot.setFire("0");
                    fire.setVisibility(View.INVISIBLE);
                    break;
            }
            return true;
        }
    };
}
