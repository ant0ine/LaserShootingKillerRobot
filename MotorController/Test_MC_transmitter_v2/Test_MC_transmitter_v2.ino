#include <Wire.h>
#include <PinChangeInterrupt.h>

/*_________________________________________________________
 * Test programm for Embedded Systems Design Project
 * Christopher McClanahan, 10.2017
 * 
 * This programm is connencted to a Transmitter, normally used for RC models. 
 * This software is used to test single systems and the whole Rover system. This
 * is necessarry, because this is a very easy program and was easy to debug, 
 * thus it is possible to determine if bugs are in the controller software or in
 * the high level controller of the raspberrie.
 __________________________________________________________*/


/* variables sent to the Laser attachments 
 * Laser1 is for the angle, Laser2 is for the Laser itself*/
int     Laser1 = 0;
int     Laser2 = 0;

/* Arrays for PWM capture */
long                    val = 0;
const byte              channel_pin[] = {7, 8, 9, 11};
volatile long           PWM[] = {0, 0, 0, 0};
volatile unsigned long  PWMstart_time[] = {0, 0, 0, 0};

/* Values recieved from Transmitter */
int     Throttle;
int     Stearing;

/* Processed Variables for transmission to Motor Controller*/
int     Left;
int     Right;
byte    Status;

/* subroutine to handle either falling or rising edges of the PWM signal  */
void readPWM(byte pin){
    uint8_t trigger = getPinChangeInterruptTrigger(digitalPinToPCINT
        (channel_pin[pin]));
     
    if(trigger == RISING){
        PWMstart_time[pin] = micros();
    }
    else if(trigger == FALLING){
        PWM[pin] = micros() - PWMstart_time[pin];
    }
    /* for debugging */ /*
    Serial.print(PWM[pin]);
    Serial.println("\t"); */
}

/* Interrupt subroutines for PWM capturing */
void change0(void){
    readPWM(0);
}
void change1(void){
    readPWM(1);
}
void change2(void){
    readPWM(2);
}
void change3(void){
    readPWM(3);
}


void setup() {

    /* Set Serial connection and initialize I2C as Master */
    Serial.begin(57600);

    Wire.begin();

    /* Set PWM channels as input */
    pinMode(channel_pin[0], INPUT);
    pinMode(channel_pin[1], INPUT);
    pinMode(channel_pin[2], INPUT);
    pinMode(channel_pin[3], INPUT);

    /* initialize Interrupts */
    attachPinChangeInterrupt(digitalPinToPinChangeInterrupt(channel_pin[0]), 
    change0, CHANGE);
    attachPinChangeInterrupt(digitalPinToPinChangeInterrupt(channel_pin[1]), 
    change1, CHANGE);
    attachPinChangeInterrupt(digitalPinToPinChangeInterrupt(channel_pin[2]), 
    change2, CHANGE);
    attachPinChangeInterrupt(digitalPinToPinChangeInterrupt(channel_pin[3]), 
    change3, CHANGE);

    /* let everything settle ;-) */
    delay(5);
} 

void loop() {
    /* for debugging */
    /*  Serial.print(PWM[0]);
    Serial.print("\t");
    Serial.print(PWM[1]);
    Serial.print("\t");
    Serial.print(PWM[2]);
    Serial.print("\t");
    Serial.print(PWM[3]);
    Serial.println("\t");
    */
   
    /* Check if we have a PWM timeout, if no continue */
    if (PWM[0]<2100, PWM[1]<2100, PWM[2]<2100, PWM[3]<2100) 
    { 
        /* convert PWM range to +-8 bits */
        Throttle = map(PWM[0], 900, 2100, 250, -250);
        Stearing = map(PWM[1], 900, 2100, 250, -250);

        /* convert stearing comands to differential drive comands */
        Left = Throttle - Stearing;
        Right = Throttle + Stearing; 

        /* constrain values to 8 bits, MC can only handle 8bit values*/
        Left = constrain(Left, -250, 250);
        Right = constrain(Right, -250, 250);

        /* set values in Status bit to transmit direction control */
        if (Left<0){
            bitSet(Status,2);
        }
        else{
            bitClear(Status,2);
        }
      
        if (Right<0){
            bitSet(Status,1);
        }
        else{
            bitClear (Status,1);
        }
       
        /*  disable motors if the control sticks are in the middle/ prevend very small 
        * veloccities */
        bitSet(Status,0);
        if (Left<50&Left>-50&Right<50&Right>-50){
            Left = 0;
            Right = 0; 
            bitClear(Status,2);   
        }
       
        /* activate/deactivate Laser */
        if (PWM[3]>1500){
            bitSet (Laser1,0);
        }
        else{
            bitClear (Laser1,0);  
        }

        /* Laser up */
        if (PWM[2]>1700){
            bitSet (Laser2,0);
            bitClear (Laser2,1);
        }

        /* Laser down */
        else if (PWM[2]<1300){
            bitSet (Laser2,1);
            bitClear (Laser2,0);
        }

        /* dont move the laser*/
        else{
            bitClear (Laser2,0);
            bitClear (Laser2,1);
        }
    }

    /* if there is a PWM timeout, set everything to LOW*/
    else {
      Laser1  = 0;
      Laser2  = 0;
      Left    = 0;
      Right   = 0;
      Status  = 0;
    }

    /* send velocity comands to MC (note, values have to be absolute, since 
    * direction is send in the Status byte */
    Wire.beginTransmission(3);
    Wire.write((byte) abs(Right));
    Wire.write((byte) abs(Left));
    Wire.write(Status);
    Wire.endTransmission();

    /* send commands to lasercannon */
    Wire.beginTransmission(5);
    Wire.write(Laser1);
    Wire.write(Laser2);
    Wire.endTransmission();

    /* tell somebody who is interested what is happening */  
    Serial.print("Left ");
    Serial.print(Left);
    Serial.print("   Right ");
    Serial.print(Right);
    Serial.print("   Status ");
    Serial.println(Status,BIN);

    /* send comands with approx. 50Hz */
    delay(20);
}
