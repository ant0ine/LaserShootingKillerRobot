/*
 * This file is the software running on the laser controller.
 * When it receives a laser shot order on the i2c bus, it triggers the Laser.
 * If it receives a 'up' or 'down' command, it will move the turret accordingly.
 * Lastly, it will handle the gyroscope data to stabilyze the camera. 
 * [NOT AVAILABLE YET]
 */

#include <Wire.h>
#include <Servo.h>

#define     PIN_LASER       A0
#define     PIN_GREEN_LED    8
#define     PIN_SERVO        9

Servo         Servo_Laser;
int           servoAngleStep;
uint8_t       Data[4];
boolean       dataAvailable;
boolean       laserActivated;
int           servoPosition;
unsigned long tLaser;
unsigned long tPulse;



void setup() 
{
    // Arduino initializations
    Wire.begin();
    Wire.onReceive(I2CRead); // register event
    pinMode(PIN_LASER, OUTPUT);
    pinMode(PIN_GREEN_LED, OUTPUT);
    /* pinMode(PIN_SERVO, OUTPUT); */
    Servo_Laser.attach(PIN_SERVO);

    // Print Init Message
    Serial.begin (57600);
    Serial.println ("Starting");

    // Hello World !
    digitalWrite(PIN_GREEN_LED, HIGH);
    delay(80);
    digitalWrite(PIN_GREEN_LED, LOW);
    delay(80);
    digitalWrite(PIN_GREEN_LED, HIGH);
    delay(80);
    digitalWrite(PIN_GREEN_LED, LOW);
    delay(80);

    // Initialising I2C, giving an adress to the slave
    Wire.begin(05);

    // Default Servo position
    servoPosition = 90;

    // Step of angle (in degree)
    servoAngleStep = 1;
    
    // Laser is off at boot
    laserActivated = false;
    
    // Init done
    Serial.println("Boot Complete !");
}

void loop() 
{
    if (dataAvailable)
    {
        // Do something about the data
        if (Data[0] == 1)
        {
            // Fire the Laser
            if (!laserActivated)
            {
                Serial.println("   FIRE !!!");
                digitalWrite(PIN_GREEN_LED, HIGH);
                digitalWrite(PIN_LASER, HIGH);
                tLaser = millis();
                tPulse = tLaser;
                laserActivated = true;
            }
        }
        if (Data[1] == 1)
        {
            // Move the camera up
            Serial.println("   Moving up !");
            servoPosition = servoPosition + servoAngleStep;
            // Set a max angle
            if (servoPosition >= 120)
            {
                servoPosition = 120;
            }
            Servo_Laser.write(servoPosition);
        }
        if (Data[1] == 2)
        {
            // Move the camera down
            Serial.println("   Moving down !");
            servoPosition = servoPosition - servoAngleStep;
            // Set a min angle
            if (servoPosition <= 60)
            {
                servoPosition = 60;
            }
            Servo_Laser.write(servoPosition);
        }
        Serial.println("   Done");

        // Reset the flag 
        dataAvailable = false;
    }

    // Pulsing of the Laser
    if (laserActivated && (tPulse - millis()) > 20)
    {
        // Switch off the laser
        digitalWrite(PIN_LASER, LOW);
        digitalWrite(PIN_GREEN_LED, LOW);
        delay(30);
        // Switch it on again to make it pulse
        tPulse = millis();
        digitalWrite(PIN_GREEN_LED, HIGH);
        digitalWrite(PIN_LASER, HIGH);
    }

    // Check when to switch off the Laser
    if (laserActivated && (tLaser-millis())>5000)
    {
        digitalWrite(PIN_GREEN_LED, LOW);
        digitalWrite(PIN_LASER, LOW);
        laserActivated = false;
    }
}

// Function that executes whenever data is received from master
// this function is registered as an event, see setup()
void I2CRead(int number)
{
    uint8_t index = 0;
    while (Wire.available())
    {
        Data[index++] = Wire.read();
    }
    dataAvailable = true;
}
