## Motor Controller
This folder is the Arduino project running on the Arduino used
as Motor Controller. It also contains the project of the 
Arduino chip used for testing of the MC. It is connected
to a RF Receiver and to the MC by I2C.
